import { Component, OnInit } from '@angular/core';
import { Project } from '../../core/project';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.css'],
})
export class ProjectViewComponent implements OnInit {
  project!: Project;

  constructor(private actRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.project = this.actRoute.snapshot.data?.['project'];
  }
}
