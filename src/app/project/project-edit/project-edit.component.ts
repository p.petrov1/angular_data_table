import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from '../../core/project';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css'],
})
export class ProjectEditComponent implements OnInit {
  public project!: Project;
  btnTitle = 'Update';

  constructor(private actRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.project = this.actRoute.snapshot.data?.['project'];
  }
}
