import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';

import { ProjectRoutingModule } from './project-routing.module';
import { ProjectComponent } from './project.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectAddComponent } from './project-add/project-add.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectFormComponent } from './shared/project-form.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ProjectViewComponent } from './project-view/project-view.component';

@NgModule({
  declarations: [
    ProjectComponent,
    ProjectListComponent,
    ProjectAddComponent,
    ProjectEditComponent,
    ProjectFormComponent,
    ProjectViewComponent,
  ],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgMultiSelectDropDownModule,
  ],
})
export class ProjectModule {}
