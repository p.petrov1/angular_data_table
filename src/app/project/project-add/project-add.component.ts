import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.css'],
})
export class ProjectAddComponent implements OnInit {
  btnTitle = 'Create';

  constructor() {}

  ngOnInit(): void {}
}
