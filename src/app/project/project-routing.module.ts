import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectAddComponent } from './project-add/project-add.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectViewComponent } from './project-view/project-view.component';
import { ProjectComponent } from './project.component';
import { ProjectResolver } from '../core/resolvers/project.resolver';

const routes: Routes = [
  {
    path: 'add',
    component: ProjectAddComponent,
  },
  {
    path: ':id',
    component: ProjectViewComponent,
    resolve: { project: ProjectResolver },
  },
  {
    path: ':id/edit',
    component: ProjectEditComponent,
    resolve: { project: ProjectResolver },
  },
  { path: '', component: ProjectComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectRoutingModule {}
