import { FormGroup } from '@angular/forms';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

export function ValidDate(startTime: string, endTime: string) {
  return (formGroup: FormGroup) => {
    const endTimeControl = formGroup.controls[endTime];

    const startValue: NgbDate = formGroup.controls[startTime].value;
    const endValue: NgbDate = endTimeControl.value;

    if (startValue !== null && endValue !== null) {
      const { year: startYear, month: startMonth, day: startDay } = startValue;
      const { year: endYear, month: endMonth, day: endDay } = endValue;

      const startDate: NgbDate = new NgbDate(startYear, startMonth, startDay);
      const endDate: NgbDate = new NgbDate(endYear, endMonth, endDay);

      if (endDate.after(startDate)) {
        endTimeControl.setErrors(null);
        return null;
      } else {
        endTimeControl.setErrors({ validator: true });
        return 'invalid';
      }
    } else {
      return null;
    }
  };
}
