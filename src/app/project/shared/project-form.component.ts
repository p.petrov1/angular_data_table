import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ValidDate } from './data-validator';
import { Project } from '../../core/project';
import { ProjectService } from '../../core/project.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
})
export class ProjectFormComponent implements OnInit {
  projectForm!: FormGroup;
  @Input() buttonName: string = '';

  model?: NgbDateStruct;
  dropdownList: Array<any> = [];
  selectedItems: Array<any> = [];
  dropdownSettings: any = {};

  categories: string[] = [];

  @Input() project: Project = {
    id: 0,
    name: '',
    description: '',
    startTime: '',
    endTime: '',
    category: '',
    users: [],
  };

  constructor(
    private fb: FormBuilder,
    private service: ProjectService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.categories = this.service.getCategories();
    this.getUserList();

    this.projectForm = this.fb.group(
      {
        name: [
          this.project.name,
          [Validators.required, Validators.pattern('^[a-zA-Z0-9 ._-]+$')],
        ],
        description: [this.project.description],
        startTime: [
          this.project.startTime
            ? this.stringToDate(this.project.startTime)
            : null,
        ],
        endTime: [
          this.project.endTime ? this.stringToDate(this.project.endTime) : null,
        ],
        category: [this.project.category, [Validators.required]],
        users: [this.project.users, [Validators.required]],
      },
      {
        validator: ValidDate('startTime', 'endTime'),
      }
    );

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'firstname',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: true,
    };
  }

  userActivateValidation(users: any) {
    users.touched = true;
  }

  getUserList() {
    this.userService.getUsers().subscribe((data: Array<any>) => {
      this.dropdownList = data;
    });
  }

  onSubmit() {
    const projectData = { ...this.projectForm.value };
    projectData.startTime = this.dateToString(projectData.startTime);
    projectData.endTime = this.dateToString(projectData.endTime);

    if (this.buttonName === 'Update') {
      const projectId: string = this.project.id.toString();
      this.service.fullUpdateProject(projectId, projectData).subscribe({
        next: () => {
          this.router.navigate(['/project']);
        },
      });
    } else if (this.buttonName === 'Create') {
      this.service.createProject(projectData).subscribe({
        next: () => {
          this.router.navigate(['/project']);
        },
      });
    } else {
      console.error('Can`t match operation, button name is unknown.');
    }
  }

  dateToString(dateObj: NgbDate): string {
    let dateToString: string = '';

    if (typeof dateObj !== 'undefined' && dateObj !== null) {
      const { year: dateYear, month: dateMonth, day: dateDay } = dateObj;

      return `${dateYear.toString()}-${dateMonth.toString()}-${dateDay.toString()}`;
    } else {
      return '';
    }
  }

  stringToDate(dateString: string): NgbDate {
    const [year, month, day] = dateString.split('-');

    return new NgbDate(Number(year), Number(month), Number(day));
  }
}
