import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../core/user';
import { UserService } from '../../core/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
})
export class UserFormComponent implements OnInit {
  userForm!: FormGroup;
  @Input() buttonName: string = '';

  @Input() user: User = {
    id: 0,
    firstname: '',
    lastname: '',
    email: '',
    username: '',
    password: '',
  };

  constructor(
    private fb: FormBuilder,
    private service: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userForm = this.fb.group({
      firstname: [this.user.firstname, Validators.required],
      lastname: [this.user.lastname, Validators.required],
      email: [this.user.email, Validators.required],
      username: [this.user.username, Validators.required],
      password: [this.user.password, Validators.required],
    });
  }

  onSubmit() {
    const userId: string = this.user.id.toString();
    const userData: User = this.userForm.value;

    if (this.buttonName === 'Update') {
      this.service.fullUpdateUser(userId, userData).subscribe({
        next: () => {
          this.router.navigate(['/user']);
        },
      });
    } else if (this.buttonName === 'Create') {
      this.service.createUser(userData).subscribe({
        next: () => {
          this.router.navigate(['/user']);
        },
      });
    } else {
      console.error('Can`t match operation, button name is unknown.');
    }
  }
}
