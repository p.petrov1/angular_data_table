import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../core/user';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
})
export class UserEditComponent implements OnInit {
  public user!: User;
  public btnTitle = 'Update';

  constructor(private actRoute: ActivatedRoute) {}

  ngOnInit() {
    this.user = this.actRoute.snapshot.data?.['user'];
  }
}
