import { Component, OnInit } from '@angular/core';
import { User } from '../../core/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css'],
})
export class UserViewComponent implements OnInit {
  user!: User;

  constructor(private actRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.user = this.actRoute.snapshot.data?.['user'];
  }
}
