import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAddComponent } from './user-add/user-add.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserViewComponent } from './user-view/user-view.component';
import { UserComponent } from './user.component';
import { UserResolver } from '../core/resolvers/user.resolver';

const routes: Routes = [
  {
    path: 'add',
    component: UserAddComponent,
  },
  {
    path: ':id',
    component: UserViewComponent,
    resolve: { user: UserResolver },
  },
  {
    path: ':id/edit',
    component: UserEditComponent,
    resolve: { user: UserResolver },
  },
  {
    path: '',
    component: UserComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
