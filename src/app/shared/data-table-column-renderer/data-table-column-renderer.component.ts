import {
  Component,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

@Component({
  selector: 'app-data-table-column-renderer',
  templateUrl: './data-table-column-renderer.component.html',
})
export class DataTableColumnRendererComponent implements OnInit {
  @Input() template!: TemplateRef<any>;

  @Input() data!: any;

  constructor(private vref: ViewContainerRef) {}

  ngOnInit(): void {
    if (this.template) {
      this.vref
        .createEmbeddedView(this.template, { data: this.data })
        .detectChanges();
    }
  }
}
