import { Component, Input, Type } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../core/user.service';
import { ProjectService } from '../core/project.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngbd-modal-confirm',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-title">
        {{ 'table_btn_delete' | translate }}
      </h4>
      <button
        type="button"
        class="btn-close"
        aria-describedby="modal-title"
        (click)="modal.dismiss('Cross click')"
      ></button>
    </div>
    <div class="modal-body">
      <p>
        <strong
          >{{ 'delete_title' | translate }}
          <span class="text-primary">"{{ this.id }}"</span></strong
        >
      </p>
      <p>
        {{ 'delete_description' | translate }}
        <span class="text-danger"> {{ 'delete_warning' | translate }}</span>
      </p>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="btn btn-outline-secondary"
        (click)="modal.dismiss('cancel click')"
      >
        {{ 'cancel' | translate }}
      </button>
      <button
        type="button"
        class="btn btn-danger"
        (click)="modal.close('Ok click')"
        (click)="delete()"
      >
        {{ 'confirm' | translate }}
      </button>
    </div>
  `,
})
export class NgbdModalConfirm {
  id: number = 0;
  urlParam: string = '';

  constructor(
    public modal: NgbActiveModal,
    private router: Router,
    private userService: UserService,
    private projectService: ProjectService
  ) {}

  delete() {
    if (this.urlParam === 'user') {
      this.userService.deleteUser(this.id.toString()).subscribe({
        next: () => {
          this.reloadCurrentRoute();
        },
      });
    } else if (this.urlParam === 'project') {
      this.projectService.deleteProject(this.id.toString()).subscribe({
        next: () => {
          this.reloadCurrentRoute();
        },
      });
    } else {
      console.error('Couldn`t find URL Match');
    }
  }

  reloadCurrentRoute() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
}

const MODALS: { [name: string]: Type<any> } = {
  focusFirst: NgbdModalConfirm,
};

@Component({
  selector: 'ngbd-modal-focus',
  template: `<button
    type="button"
    class="btn btn-danger ms-2 btn-sm float-end"
    (click)="open('focusFirst')"
  >
    {{ 'table_btn_delete' | translate }}
  </button>`,
})
export class NgbdModalFocus {
  @Input() id: number = 0;
  @Input() urlParam: string = '';

  constructor(private _modalService: NgbModal) {}

  open(name: string) {
    const modalRef = this._modalService.open(MODALS[name]);

    modalRef.componentInstance.id = this.id;
    modalRef.componentInstance.urlParam = this.urlParam;
  }
}
