import { Observable } from 'rxjs';
import { User } from '../../core/user';
import { Project } from '../../core/project';

export interface DataService {
  getData(
    searchTerm?: string,
    page?: string,
    pageSize?: string,
    sortBy?: string
  ): Observable<User[] | Project[]>;
}
