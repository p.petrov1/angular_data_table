import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContentChildren, QueryList } from '@angular/core';
import { DataTableColumnComponent } from '../data-table-column/data-table-column.component';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
})
export class DataTableComponent implements OnInit {
  @ContentChildren(DataTableColumnComponent)
  columns!: QueryList<DataTableColumnComponent>;

  @Input()
  dataService: any;

  public data: any = [];
  public total: number = 0;

  public Page: number = 1;
  public PageSize: number = 4;
  public Filter: string = '';
  public sort: string = '';

  public url: string = '';
  public loading = true;

  constructor(public router: Router) {}

  ngOnInit(): void {
    this.url = this.router.url.slice(1);
    this.fetchTotal();
    this.dataFetch();
  }

  get columnsNames(): any[] {
    return this.columns ? this.columns.map((p) => p.name) : [];
  }

  get columnsSort(): any[] {
    return this.columns
      ? this.columns.map((p) => {
          if (p.sort === 'enabled') {
            return true;
          } else if (p.sort === 'disabled') {
            return false;
          } else {
            return true;
          }
        })
      : [];
  }

  arrow(columnName: string) {
    let isDesc = '';
    if (this.sort === columnName) {
      return isDesc ? '&#129045;' : '&#129047;';
    }
    return '&#129045;';
  }

  sortBy(header: string) {
    this.sort === header ? (this.sort = '') : (this.sort = header);
    this.dataFetch();
  }

  onPageChange(page: number) {
    this.Page = page;
    this.dataFetch();
  }

  onSearch() {
    this.fetchTotal();
    this.dataFetch();
  }

  onPageSizeChange() {
    if (this.Page <= Math.ceil(this.total / this.PageSize)) {
      this.dataFetch();
    }
  }

  private dataFetch() {
    this.loading = true;
    this.dataService
      .getData(
        this.Filter,
        this.Page.toString(),
        this.PageSize.toString(),
        this.sort
      )
      .subscribe((data: any) => {
        this.loading = false;
        this.data = data;
      });
  }

  private fetchTotal() {
    this.loading = true;
    this.dataService.getData(this.Filter).subscribe((data: any) => {
      this.loading = false;
      this.total = data.length;
    });
  }
}
