import {
  Component,
  ContentChild,
  Input,
  OnInit,
  TemplateRef,
} from '@angular/core';

@Component({
  selector: 'app-data-table-column',
  template: ``,
})
export class DataTableColumnComponent implements OnInit {
  @ContentChild(TemplateRef) template!: TemplateRef<any>;

  @Input() name!: string;

  @Input() sort!: string;

  constructor() {}

  ngOnInit(): void {}
}
