import { NgModule } from '@angular/core';
import { NgbdModalConfirm, NgbdModalFocus } from './modal-focus';
import { DataTableComponent } from './data-table/data-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { DataTableColumnComponent } from './data-table-column/data-table-column.component';
import { DataTableColumnRendererComponent } from './data-table-column-renderer/data-table-column-renderer.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NgbdModalConfirm,
    NgbdModalFocus,
    DataTableComponent,
    DataTableColumnComponent,
    DataTableColumnRendererComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
  ],
  exports: [
    NgbdModalFocus,
    DataTableComponent,
    DataTableColumnComponent,
    TranslateModule,
  ],
  bootstrap: [NgbdModalFocus],
})
export class SharedModule {}
