import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, Observable, map } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class FetchService {
  constructor(private http: HttpClient, private toastr: ToastrService) {}

  // GET DATA
  get(path: string): Observable<any> {
    return this.http.get(path).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof Error) {
          this.showError(error.error.message, 'Error');
        } else {
          this.showError(error.error, error.status.toString());
        }
        return '';
      }),
      map((data) => {
        this.showSuccess('Fetch succesfull', 'Fetch');
        return data;
      })
    );
  }

  // CREATE
  post(path: string, data: any): Observable<any> {
    return this.http.post(path, data).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof Error) {
          this.showError(error.error.message, 'Error');
        } else {
          this.showError(error.error, error.status.toString());
        }
        return '';
      }),
      map((data) => {
        this.showWarning('Created successfully', 'Create');
        return data;
      })
    );
  }

  // UPDATE
  put(path: string, data: any): Observable<any> {
    return this.http.put(path, data).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof Error) {
          this.showError(error.error.message, 'Error');
        } else {
          this.showError(error.error, error.status.toString());
        }
        return '';
      }),
      map((data) => {
        this.showWarning('Updated successfully', 'Update');
        return data;
      })
    );
  }

  // UPDATE
  patch(path: string, data: any): Observable<any> {
    return this.http.patch(path, data).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof Error) {
          this.showError(error.error.message, 'Error');
        } else {
          this.showError(error.error, error.status.toString());
        }
        return '';
      }),
      map((data) => {
        this.showWarning('Updated successfully', 'Update');
        return data;
      })
    );
  }

  // DELETE
  delete(path: string, id: string): Observable<any> {
    const url = `${path}/${id}`;
    return this.http.delete(url).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof Error) {
          this.showError(error.error.message, 'Error');
        } else {
          this.showError(error.error, error.status.toString());
        }
        return '';
      }),
      map((data) => {
        this.showWarning('Deleted successfully', 'Delete');
        return data;
      })
    );
  }

  private showSuccess(message: string, title: string) {
    this.toastr.success(message, title, { timeOut: 1000 });
  }

  private showError(message: string, title: string) {
    this.toastr.error(message, title, { timeOut: 3000 });
  }

  private showWarning(message: string, title: string) {
    this.toastr.warning(message, title, { timeOut: 3000 });
  }
}
