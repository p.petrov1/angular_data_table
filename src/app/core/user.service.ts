/* eslint-disable @typescript-eslint/adjacent-overload-signatures */
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

import { User } from './user';
import { FetchService } from './fetch.service';

import { DataService } from '../shared/data-table/data-service.interface';

@Injectable({ providedIn: 'root' })
export class UserService implements DataService {
  private url = 'https://61f39cd610f0f7001768c51d.mockapi.io/User';

  constructor(private fetchUser: FetchService) {}

  // Finds if User ID exist and display in user detail page.

  User(id?: number | string) {
    const test = this.getUsers('', '15').pipe(map((data): User[] => data));

    return test.pipe(
      map(
        (users: User[]) =>
          users.find((user) => {
            return user.id === id;
          })!
      )
    );
  }

  // -------------- CRUD User methods ---------------- //

  getUser(id: string): Observable<any> {
    // Find user by ID;
    const path = `${this.url}${'/' ? id : ''}${id}`;
    return this.fetchUser.get(path);
  }

  getUsers(
    searchTerm?: string,
    page?: string,
    pageSize?: string,
    sortBy?: string
  ): Observable<User[]> {
    // Find all users by params;
    page = page ? page : '';
    pageSize = pageSize ? pageSize : '';
    searchTerm = searchTerm ? searchTerm : '';
    sortBy = sortBy ? sortBy : '';

    const path = `${this.url}?p=${page}&l=${pageSize}&search=${searchTerm}&sortBy=${sortBy}`;

    return this.fetchUser.get(path);
  }

  createUser(data: User): Observable<any> {
    // Create user w given data;
    return this.fetchUser.post(this.url, data);
  }

  fullUpdateUser(id: string, data: User): Observable<any> {
    // Update user w new data;
    const updatePath: string = `${this.url}/${id}`;
    return this.fetchUser.put(updatePath, data);
  }

  updateUser(id: string, data: any): Observable<any> {
    // Update user field w new field;
    const updatePath: string = `${this.url}/${id}`;
    return this.fetchUser.patch(updatePath, data);
  }

  deleteUser(id: string): Observable<any> {
    // Delete user by ID;
    return this.fetchUser.delete(this.url, id);
  }

  getData(
    searchTerm?: string,
    page?: string,
    pageSize?: string,
    sortBy?: string
  ) {
    return this.getUsers(searchTerm, page, pageSize, sortBy);
  }
  // -------------------------------------------- //
}
