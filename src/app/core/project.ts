export interface Project {
  id: number;
  name: string;
  description: string;
  startTime: string;
  endTime: string;
  category: string;
  users: Array<any>;
}
