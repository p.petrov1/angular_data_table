/* eslint-disable @typescript-eslint/adjacent-overload-signatures */
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

import { Project } from './project';
import { FetchService } from './fetch.service';

import { DataService } from '../shared/data-table/data-service.interface';

@Injectable({ providedIn: 'root' })
export class ProjectService implements DataService {
  private url = 'https://61f39cd610f0f7001768c51d.mockapi.io/Project';
  private categories = ['Maintenance', 'Marketing', 'Business'];

  constructor(private fetchProject: FetchService) {}

  Project(id: number | string) {
    const data = this.getProjects('', '15').pipe(
      map((data): Project[] => data)
    );
    return data.pipe(
      map(
        (projects: Project[]) =>
          projects.find((projects) => {
            return projects.id === id;
          })!
      )
    );
  }

  // -------------- CRUD Project methods ---------------- //
  getCategories() {
    return this.categories;
  }

  getProject(id: string): Observable<any> {
    // Find project by ID;
    const path = `${this.url}${'/' ? id : ''}${id}`;
    return this.fetchProject.get(path);
  }

  getProjects(
    searchTerm?: string,
    page?: string,
    pageSize?: string,
    sortBy?: string
  ): Observable<Project[]> {
    // Find all projects by params;
    page = page ? page : '';
    pageSize = pageSize ? pageSize : '';
    searchTerm = searchTerm ? searchTerm : '';
    sortBy = sortBy ? sortBy : '';

    const path = `${this.url}?p=${page}&l=${pageSize}&search=${searchTerm}&sortBy=${sortBy}`;

    return this.fetchProject.get(path);
  }

  createProject(data: Project): Observable<any> {
    // Create project w given data;
    return this.fetchProject.post(this.url, data);
  }

  fullUpdateProject(id: string, data: Project): Observable<any> {
    // Update project w new data;
    const updatePath: string = `${this.url}/${id}`;
    return this.fetchProject.put(updatePath, data);
  }

  updateProject(id: string, data: any): Observable<any> {
    // Update project field w new field;
    const updatePath: string = `${this.url}/${id}`;
    return this.fetchProject.patch(updatePath, data);
  }

  deleteProject(id: string): Observable<any> {
    // Delete project by ID;
    return this.fetchProject.delete(this.url, id);
  }

  getData(
    searchTerm?: string,
    page?: string,
    pageSize?: string,
    sortBy?: string
  ) {
    return this.getProjects(searchTerm, page, pageSize, sortBy);
  }

  // -------------------------------------------- //
}
