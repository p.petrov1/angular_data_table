import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../user';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserResolver implements Resolve<any> {
  constructor(private UserService: UserService, private Router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const url = state.url.slice(1).split('/')[0];

    if (url === 'user') {
      return this.UserService.User(route.paramMap.get('id')!).pipe(
        map((userData: User) => {
          if (typeof userData === 'undefined') {
            this.Router.navigateByUrl('/user');
            return userData;
          } else {
            return userData;
          }
        })
      );
    } else {
      this.Router.navigateByUrl('/');
    }
  }
}
