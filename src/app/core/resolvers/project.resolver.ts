import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { ProjectService } from '../project.service';
import { Project } from '../project';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProjectResolver implements Resolve<any> {
  constructor(private ProjectService: ProjectService, private Router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const url = state.url.slice(1).split('/')[0];

    if (url === 'project') {
      return this.ProjectService.Project(route.paramMap.get('id')!).pipe(
        map((projectData: Project) => {
          if (typeof projectData === 'undefined') {
            this.Router.navigateByUrl('/project');
            return projectData;
          } else {
            return projectData;
          }
        })
      );
    } else {
      this.Router.navigateByUrl('/');
    }
  }
}
